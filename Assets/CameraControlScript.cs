﻿﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControlScript : MonoBehaviour {

	public float angle; 
	public float weight;

	private float angleRoom;
	private bool sInputReady = true;
	private bool potSelected = false;
	private GameObject selectedPot;

	void Start () {
		transform.RotateAround(Vector3.zero, Vector3.zero, -90);
		transform.LookAt(GameObject.Find("Plane").transform);
	}

	void Update () {
		if (gameObject.transform.position.y < 13) {
			gameObject.transform.position = new Vector3(gameObject.transform.position.x, 13, gameObject.transform.position.z);
		}
		else {
			float xInput = Input.GetAxis("Horizontal");
			float sInput = Input.GetAxis("Vertical");
			float zoomInput = Input.GetAxis("Mouse ScrollWheel");

			float xdeltaAngle = -xInput * Time.deltaTime * weight;
			angle += xdeltaAngle;
			transform.RotateAround(Vector3.zero, Vector3.up, xdeltaAngle);
			float ydeltaAngle = sInput * Time.deltaTime * weight;
			angle += ydeltaAngle;
			transform.Translate(Vector3.up * ydeltaAngle, Space.Self);

			float zoomdeltaAngle = zoomInput * Time.deltaTime * weight * 100;
			angle += zoomdeltaAngle;
			transform.Translate(Vector3.forward * zoomdeltaAngle, Space.Self);

			//transform.RotateAround(Vector3.zero, Vector3.left, ydeltaAngle);
			transform.LookAt(GameObject.Find("Plane").transform);
		}
		
		
	}

}
