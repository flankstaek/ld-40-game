﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartMenuScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void startButtonOnClick() {
		SceneManager.LoadScene(1);
	}
	public void tutorialButtonOnClick() {
		SceneManager.LoadScene(3);
	}
}
