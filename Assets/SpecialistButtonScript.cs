﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpecialistButtonScript : MonoBehaviour {

	public GameManagerScript gameManager;
	// Use this for initialization
	void Start () {
		gameManager = GameObject.Find("ManagerObject").GetComponent<GameManagerScript>();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown("1")) {
			usingPolice();
		}
		if (Input.GetKeyDown("2")) {
			usingDoctor();
		}
		if (Input.GetKeyDown("3")) {
			usingFireman();
		}
	}

	public void usingPolice() {
		if (GameManagerScript.usingPolice) {
			GameManagerScript.usingPolice = false;
		}
		else if (gameManager.policeSpecialists>0){
			GameManagerScript.usingPolice = true;
			GameManagerScript.usingDoctor = false;
			GameManagerScript.usingFireman = false;
		}
	}
	public void usingDoctor() {
		if (GameManagerScript.usingDoctor) {
			GameManagerScript.usingDoctor = false;
		}
		else if (gameManager.doctorSpecialists>0) {
			GameManagerScript.usingDoctor = true;
			GameManagerScript.usingFireman = false;
			GameManagerScript.usingPolice = false;
		}
	}
	public void usingFireman() {
		if (GameManagerScript.usingFireman) {
			GameManagerScript.usingFireman = false;
		}
		else if (gameManager.firemanSpecialists>0) {
			GameManagerScript.usingFireman = true;
			GameManagerScript.usingDoctor = false;
			GameManagerScript.usingPolice = false;
		}
		
	}


}
