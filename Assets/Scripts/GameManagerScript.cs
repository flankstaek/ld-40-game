﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManagerScript : MonoBehaviour {
	public static bool usingPolice = false;	
	public static bool usingDoctor = false;
	public static bool usingFireman = false;

	public Button policeButton;
	public Button doctorButton;
	public Button firemanButton;
	public Text scoreText;
	public GameObject eventWindow;
	public int policeSpecialists = 0;
	public int firemanSpecialists = 0;
	public int doctorSpecialists = 0;
	public int policeProgress = 0;
	public int firemanProgress = 0;
	public int doctorProgress = 0;
	public int firehouseCount = 0;
	public int policestationCount = 0;
	public int hospitalCount = 0;
	public int apartmentCount = 0;
	public int spreadLikelyhood = 10;
	public int specialistProgressCap = 100;
	public int maxEventTimeInterval = 10;

	public float timeUntilEvent;
	public int population = 0;
	public int score = 0;
	public static int recentHighScore = 0;
	public static int[] highScores = new int[10];
	Tile[,] tiles;
	GameObject[,] renderedTiles = new GameObject[10, 10];

	public GameObject map;

	List<GameObject> currentEvents = new List<GameObject>();
	public bool tutorial = false;
	public int statusDuration;

	public Tile tutorialApartment;
	public Tile tutorialFirehouse;
	public Tile tutorialHospital;
	public Tile tutorialPolice;
	public GameObject tutorialBox;
	private int tutorialStep = 0;

	public List<string> tutorialMessages = new List<string>();
	// Use this for initialization
	void Start () {
		Vector3 TS = map.GetComponent<Terrain>().terrainData.size;
 		map.transform.position = new Vector3(-TS.x/2, 4f, -TS.z/2);
		for(int i = 0; i < highScores.Length; i++) {
			highScores[i] = PlayerPrefs.GetInt("Score" + i);
		}
		if (!tutorial) {
			tiles = CityManagerScript.generateTiles();
			for (int x = 0; x < 10; x++) {
				for (int y = 0; y < 10; y++) {
					GameObject tilePrefab = Instantiate((GameObject)Resources.Load(tiles[x,y].prefabName));
					tilePrefab.transform.position = tiles[x,y].pos;
					tilePrefab.GetComponent<Tile>().pos = tiles[x,y].pos;
					tilePrefab.GetComponent<Tile>().xcoord = x;
					tilePrefab.GetComponent<Tile>().ycoord = y;
					renderedTiles[x, y] = tilePrefab;
				}
			}
			timeUntilEvent = 5;
		}
		else {
			startTutorialText();
		}
		
	}

	
	// Update is called once per frame
	void Update () {
		if (!tutorial) {
			scoreText.text = "Score: " + score;
			timeUntilEvent -= Time.deltaTime;
			if ( timeUntilEvent <= 0 )
			{
				CityManagerScript.startEvent(renderedTiles);
				timeUntilEvent = Random.Range(0, maxEventTimeInterval);
			}
			
			if (apartmentCount == 0 && hospitalCount == 0 && firehouseCount == 0 && policestationCount == 0) {
				recentHighScore = score;
				SceneManager.LoadScene(2);
			}
		}
		updateUI();
		
	}

	public void startTutorialText() {
		tutorialMessages.Add("Welcome Awfultown!\nClick Here to progress the tutorial!");
		tutorialMessages.Add("These are the four building types.");
		tutorialMessages.Add("Apartments provide 100 population for the city. This contributes to your score every second.");
		tutorialMessages.Add("The Firehouse provides 50 population and helps produce Firemen.");
		tutorialMessages.Add("The Hospital provides 50 populatioin and helps produce Doctors.");
		tutorialMessages.Add("The Police Station provides 50 population and helps produce Police.");
		tutorialMessages.Add("Throughout the game bad things will happen to your buildings."); //6
		tutorialMessages.Add("You can use your specialists to resolve these problems!\nClick the relevant button on the side of your screen or use your number keys 1-3 to select a specialist,\n then click on the buliding you would like to send them to!");
		tutorialMessages.Add("Police solve riots (red frowny faces above a building).");
		tutorialMessages.Add("Doctors solve sickness (green bubbles above a building).");
		tutorialMessages.Add("Firemen solve fires (black smoke above a building).");
		tutorialMessages.Add("The amount of each specialist you currently have is shown on the relevant specialist button in the top left.");
		tutorialMessages.Add("The sliding bar at the bottom shows your progress to getting another of that specialist.");
		tutorialMessages.Add("The different building contribute to how fast you get your specialists, and you can't have more specialists than buildings of that type!");
		tutorialMessages.Add("Statuses affecting buildings might spread if left untended to.");
		tutorialMessages.Add("If you don't have any specialists to solve the problem, you can Click a building to demolish it.\nThis will keep the problem from spreading, but will lower your population.");
		tutorialMessages.Add("Buildings have a chance at resolving their own problems.\nIf you don't want to destroy a building, you can wait and see if the problem just goes away!");
		tutorialMessages.Add("You will eventually run out of buildings, and that's okay! Try to keep the highest population you can for as long as possible to help your score!");
		tutorialMessages.Add("For better control, use the WASD keys to move the camera, and 1-3 to select your specialists, and the mousewheel to zoom the camera.");
		tutorialMessages.Add("Have fun going for the highscore! And thank you for playing Awfultown!\n:)");
		tutorialBox.GetComponentInChildren<Text>().text = tutorialMessages[tutorialStep];
	}

	public void TutorialBoxOnClick() {
		tutorialStep++;
		if (tutorialStep < tutorialMessages.Count) {
			tutorialBox.GetComponentInChildren<Text>().text = tutorialMessages[tutorialStep];
		}
		else {
			SceneManager.LoadScene(0);
		}
		switch (tutorialStep) {
			case 6:
				tutorialApartment.FireEvent();
				tutorialHospital.VirusEvent();
				tutorialFirehouse.RiotEvent();
				break;
			default:
				break;
		}
	}

	public void updateUI() {
		if (policeSpecialists>policestationCount) {
			policeSpecialists = policestationCount;
		}
		if (doctorSpecialists>hospitalCount) {
			doctorSpecialists = hospitalCount;
		}
		if (firemanSpecialists>firehouseCount) {
			firemanSpecialists = firehouseCount;
		}
		if (policeProgress >= specialistProgressCap && policeSpecialists<policestationCount) {
			policeSpecialists++;
			policeProgress = 0;
		}
		if (doctorProgress >= specialistProgressCap && doctorSpecialists<hospitalCount) {
			doctorSpecialists++;
			doctorProgress = 0;
		}
		if (firemanProgress >= specialistProgressCap && firemanSpecialists<firehouseCount) {
			firemanSpecialists++;
			firemanProgress = 0;
		}
		if (policestationCount == 0) {
			policeProgress = 0;
		}
		if (firehouseCount== 0) {
			firemanProgress = 0;
		}
		if (hospitalCount == 0) {
			doctorProgress = 0;
		}
		policeButton.GetComponentInChildren<Slider>().maxValue = specialistProgressCap;
		doctorButton.GetComponentInChildren<Slider>().maxValue = specialistProgressCap;
		firemanButton.GetComponentInChildren<Slider>().maxValue = specialistProgressCap;
		policeButton.GetComponentInChildren<Slider>().value = policeProgress;
		policeButton.GetComponentInChildren<Text>().text = policeSpecialists.ToString();
		doctorButton.GetComponentInChildren<Slider>().value = doctorProgress;
		doctorButton.GetComponentInChildren<Text>().text = doctorSpecialists.ToString();
		firemanButton.GetComponentInChildren<Slider>().value = firemanProgress;
		firemanButton.GetComponentInChildren<Text>().text = firemanSpecialists.ToString();
		if (usingPolice) {
			policeButton.GetComponent<Image>().color = Color.green;
		}
		else {
			policeButton.GetComponent<Image>().color = Color.white;
		}
		if (usingDoctor) {
			doctorButton.GetComponent<Image>().color = Color.green;
		}
		else {
			doctorButton.GetComponent<Image>().color = Color.white;
		}
		if (usingFireman) {
			firemanButton.GetComponent<Image>().color = Color.green;
		}
		else {
			firemanButton.GetComponent<Image>().color = Color.white;
		}
	}

	public void riskNeighborBuildings(int x, int y, string destroyedBy) {
		if (destroyedBy != "player" && !tutorial) {
			if (x > 0 && x < 9) {
				if (y > 0 && y < 9) {
					if (Random.Range(0, spreadLikelyhood)!=0) {
						renderedTiles[x-1, y].GetComponent<Tile>().startSpecificEvent(destroyedBy);
					}
					if (Random.Range(0, spreadLikelyhood)!=0) {
						renderedTiles[x+1, y].GetComponent<Tile>().startSpecificEvent(destroyedBy);
					}
					if (Random.Range(0, spreadLikelyhood)!=0) {
						renderedTiles[x, y-1].GetComponent<Tile>().startSpecificEvent(destroyedBy);
					}
					if (Random.Range(0, spreadLikelyhood)!=0) {
						renderedTiles[x, y+1].GetComponent<Tile>().startSpecificEvent(destroyedBy);
					}
				}
				else if (y == 0) {
					if (Random.Range(0, spreadLikelyhood)!=0) {
						renderedTiles[x-1, y].GetComponent<Tile>().startSpecificEvent(destroyedBy);
					}
					if (Random.Range(0, spreadLikelyhood)!=0) {
						renderedTiles[x+1, y].GetComponent<Tile>().startSpecificEvent(destroyedBy);
					}
					if (Random.Range(0, spreadLikelyhood)!=0) {
						renderedTiles[x, y+1].GetComponent<Tile>().startSpecificEvent(destroyedBy);
					}
				}
				else {
					if (Random.Range(0, spreadLikelyhood)!=0) {
						renderedTiles[x-1, y].GetComponent<Tile>().startSpecificEvent(destroyedBy);
					}
					if (Random.Range(0, spreadLikelyhood)!=0) {
						renderedTiles[x+1, y].GetComponent<Tile>().startSpecificEvent(destroyedBy);
					}
					if (Random.Range(0, spreadLikelyhood)!=0) {
						renderedTiles[x, y-1].GetComponent<Tile>().startSpecificEvent(destroyedBy);
					}
				}
			}
			else if (x == 0) {
				if (y > 0 && y < 9) {
					if (Random.Range(0, spreadLikelyhood)!=0) {
						renderedTiles[x+1, y].GetComponent<Tile>().startSpecificEvent(destroyedBy);
					}
					if (Random.Range(0, spreadLikelyhood)!=0) {
						renderedTiles[x, y-1].GetComponent<Tile>().startSpecificEvent(destroyedBy);
					}
					if (Random.Range(0, spreadLikelyhood)!=0) {
						renderedTiles[x, y+1].GetComponent<Tile>().startSpecificEvent(destroyedBy);
					}
				}
				else if (y == 0) {
					if (Random.Range(0, spreadLikelyhood)!=0) {
						renderedTiles[x+1, y].GetComponent<Tile>().startSpecificEvent(destroyedBy);
					}
					if (Random.Range(0, spreadLikelyhood)!=0) {
						renderedTiles[x, y+1].GetComponent<Tile>().startSpecificEvent(destroyedBy);
					}
				}
				else {
					if (Random.Range(0, spreadLikelyhood)!=0) {
						renderedTiles[x+1, y].GetComponent<Tile>().startSpecificEvent(destroyedBy);
					}
					if (Random.Range(0, spreadLikelyhood)!=0) {
						renderedTiles[x, y-1].GetComponent<Tile>().startSpecificEvent(destroyedBy);
					}
				}
			}
			else {
				if (y > 0 && y < 9) {
					if (Random.Range(0, spreadLikelyhood)!=0) {
						renderedTiles[x-1, y].GetComponent<Tile>().startSpecificEvent(destroyedBy);
					}
					if (Random.Range(0, spreadLikelyhood)!=0) {
						renderedTiles[x, y-1].GetComponent<Tile>().startSpecificEvent(destroyedBy);
					}
					if (Random.Range(0, spreadLikelyhood)!=0) {
						renderedTiles[x, y+1].GetComponent<Tile>().startSpecificEvent(destroyedBy);
					}
				}
				else if (y == 0) {
					if (Random.Range(0, spreadLikelyhood)!=0) {
						renderedTiles[x-1, y].GetComponent<Tile>().startSpecificEvent(destroyedBy);
					}
					if (Random.Range(0, spreadLikelyhood)!=0) {
						renderedTiles[x, y+1].GetComponent<Tile>().startSpecificEvent(destroyedBy);
					}
				}
				else {
					if (Random.Range(0, spreadLikelyhood)!=0) {
						renderedTiles[x-1, y].GetComponent<Tile>().startSpecificEvent(destroyedBy);
					}
					if (Random.Range(0, spreadLikelyhood)!=0) {
						renderedTiles[x, y-1].GetComponent<Tile>().startSpecificEvent(destroyedBy);
					}
				}
			}
		}
	}
}
