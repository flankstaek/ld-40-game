﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
[RequireComponent(typeof(AudioSource))]
public class Tile : MonoBehaviour {
	public enum Name {Apartment, Police, Firehouse, Hospital, Rubble};
	public Name name;
	public Vector3 pos;
	public int xcoord;
	public int ycoord;
	public string prefabName;

	public string status;
	public string destroyedBy;
	public float eventTimer = 5;
	public GameManagerScript gameManager;
	public bool paused = false;
	private int nextUpdate = 1;
	public ParticleSystem smoke;
	public ParticleSystem virus;
	public ParticleSystem riot;
	public int populationValue = 100;

	public AudioClip yay;
	public AudioClip ohno;
	public AudioClip phew;
	public AudioClip fire;
	public AudioClip boo;
	public AudioClip bleh;
	AudioSource audioSource;
	// Use this for initialization
	void Start () {
		
		audioSource = GetComponent<AudioSource>();
		this.status = "fine";
		this.gameManager = GameObject.Find("ManagerObject").GetComponent<GameManagerScript>();
		eventTimer = this.gameManager.statusDuration;
		this.gameManager.population += populationValue;
		switch (this.name) {
			case Name.Firehouse:
				this.gameManager.firehouseCount++;
				populationValue = 50;
				break;
			case Name.Police:
				this.gameManager.policestationCount++;
				populationValue = 50;
				break;
			case Name.Hospital:
				this.gameManager.hospitalCount++;
				populationValue = 50;
				break;
			default: 
				this.gameManager.apartmentCount++;
				break;
		}
		gameObject.GetComponentsInChildren<MeshRenderer>()[2].enabled = false;
	}

	public void destroyTile(string destroyer) {
		if (status != "destroyed") {
			destroyedBy = status;
			status = "destroyed";
			print("A "+ this.name.ToString() + " is destroyed!");
			//CHANGE TO RUBBLE
			gameObject.GetComponentsInChildren<MeshRenderer>()[1].enabled = false;
			transform.Find("foliage").gameObject.SetActive(false);
			gameObject.GetComponentsInChildren<MeshRenderer>()[2].enabled = true;
			
			// for (int i = 1; i < gameObject.GetComponentsInChildren<MeshRenderer>().Length; i++) {
			// 	gameObject.GetComponentsInChildren<MeshRenderer>()[i].enabled = false;
			// }
			if (destroyer != "player") {
				gameManager.riskNeighborBuildings(xcoord, ycoord, destroyedBy);
			}
			this.paused = true;
			switch (this.name) {
				case Name.Firehouse:
					this.gameManager.firehouseCount--;
					break;
				case Name.Police:
					this.gameManager.policestationCount--;
					break;
				case Name.Hospital:
					this.gameManager.hospitalCount--;
					break;
				default: 
					this.gameManager.apartmentCount--;
					break;
			}
			smoke.Stop();
			virus.Stop();
			riot.Stop();
			gameManager.population -= populationValue;
			audioSource.PlayOneShot(ohno, 0.5F);
		}
	}

	void Update() {
		if (!paused) {
			if (status != "fine" && !gameManager.tutorial) {
				eventTimer -= Time.deltaTime;
				if ( eventTimer <= 0 )
				{
					destroyTile(status);
				}
			}
			if(Time.time>=nextUpdate){
				nextUpdate=Mathf.FloorToInt(Time.time)+1;
				gameManager.score += populationValue;
				if (status != "fine" && !gameManager.tutorial) {
					//chance of burning building to solve itself
					int r = Random.Range(0, 100);
					if (r < 6) {
						this.status = "fine";
						eventTimer = gameManager.statusDuration;
						print("A BUILDING SAVED ITSELF");
						smoke.Stop();
						virus.Stop();
						riot.Stop();
						audioSource.PlayOneShot(phew, 0.1f);
					}
				}
				
				switch(this.name) {
					case Name.Firehouse:
						gameManager.firemanProgress += 1;
						break;
					case Name.Hospital:
						gameManager.doctorProgress += 1;
						break;
					case Name.Police:
						gameManager.policeProgress += 1;
						break;
					default:
						break;
				}
				
			}
		}
		if (status == "destroyed" && gameObject.GetComponentsInChildren<BoxCollider>()[1].enabled) {
			gameObject.GetComponentsInChildren<BoxCollider>()[1].enabled = false;
		}
		if(Input.GetMouseButtonDown(0))
        {
             RaycastHit hit;
             Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
             if (Physics.Raycast(ray, out hit))
             { 
                if( hit.collider == gameObject.GetComponentsInChildren<Collider>()[1] || hit.collider == gameObject.GetComponentsInChildren<Collider>()[0])
                {
                     this.TileClicked();
				}
			 }
		 }
	}
	
	public void TileClicked()
    {
		if (GameManagerScript.usingPolice && status == "rioting") {
			gameManager.policeSpecialists--;
			status = "fine";
			eventTimer = gameManager.statusDuration;
			GameManagerScript.usingPolice = false;
			riot.Stop();
			audioSource.PlayOneShot(yay, 0.7F);
			
		}
		else if (GameManagerScript.usingPolice) {
			gameManager.policeSpecialists--;
			GameManagerScript.usingPolice = false;
		}
		else if (GameManagerScript.usingDoctor && status == "infected") {
			gameManager.doctorSpecialists--;
			status = "fine";
			eventTimer = gameManager.statusDuration;
			GameManagerScript.usingDoctor = false;
			virus.Stop();
			audioSource.PlayOneShot(yay, 0.7F);
		}
		else if (GameManagerScript.usingDoctor) {
			gameManager.doctorSpecialists--;
			GameManagerScript.usingDoctor = false;
		}
		else if (GameManagerScript.usingFireman && status == "burning") {
			gameManager.firemanSpecialists--;
			status = "fine";
			eventTimer = gameManager.statusDuration;
			GameManagerScript.usingFireman = false;
			smoke.Stop();
			audioSource.PlayOneShot(yay, 0.7F);
		}
		else if (GameManagerScript.usingFireman) {
			gameManager.firemanSpecialists--;
			GameManagerScript.usingFireman = false;
		}
		else {
 			destroyTile("player");
		}
       
    }

	public Tile(Tile.Name n) {
		this.name = n;
		if (this.name == Name.Apartment) {
			this.prefabName = "ApartmentTilePrefab";
		}
		else if (this.name == Name.Police) {
			this.prefabName = "PoliceTilePrefab";
		}
		else if (this.name == Name.Firehouse) {
			this.prefabName = "FirehouseTilePrefab";
		}
		else {
			this.prefabName = "HospitalTilePrefab";
		}
		this.status = "fine";
	}

	public void randomEvent() {
		if (status != "destroyed") {
			switch (Random.Range(0, 4)) {
				case 0:
					this.FireEvent();
					break;
				case 1:
					this.RiotEvent();
					break;
				case 2:
					this.VirusEvent();
					break;
				default:
					this.FireEvent();
					break;
			}
			print("A "+ this.name.ToString() + " is " + this.status);
			eventTimer = gameManager.statusDuration;
		}
	}

	public void startSpecificEvent(string eventName) {
		if (status == "fine") {
			switch (eventName) {
				case "burning":
					FireEvent();
					break;
				case "rioting":
					RiotEvent();
					break;
				case "infected":
					VirusEvent();
					break;
				default: break;
			}
			eventTimer = gameManager.statusDuration;
			print("A "+ this.name.ToString() + " is " + this.status);
		}
	}

	public void FireEvent() {
		this.status = "burning";
		smoke.Play();
		audioSource.PlayOneShot(fire, 0.1F);
		//gameManager.createEventNotification("FIRE!", new Vector2(xcoord,ycoord));
	}

	public void RiotEvent() {
		this.status = "rioting";
		riot.Play();
		audioSource.PlayOneShot(boo, 0.1F);
		//gameManager.createEventNotification("FIRE!", new Vector2(xcoord,ycoord));
	}

	public void VirusEvent() {
		this.status = "infected";
		virus.Play();
		audioSource.PlayOneShot(bleh, 0.1F);
		//gameManager.createEventNotification("FIRE!", new Vector2(xcoord,ycoord));
	}
}
