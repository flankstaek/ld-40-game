﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CityManagerScript : MonoBehaviour {
	static int width = 10;
	static int height = 10;
	public static Tile[,] generateTiles(){
		Tile temp;
		Tile[,] tiles = new Tile[width, height];
		for (int x = 0; x < 10; x++) {
			for (int y = 0; y < 10; y++) {
				temp = generateTile();
				temp.pos.x = (-width/2f) * width + width/2f + x * width;
				temp.pos.z = (-height/2f) * height + height/2f + y * height;
				tiles[x,y] = temp;
			}
		}
		return tiles;
	}
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	public static Tile generateTile(){
		int n = Random.Range(0, 100);
		if (n < 70) {
			return new Tile(Tile.Name.Apartment);
		}
		else if (n < 80) {
			return new Tile(Tile.Name.Police);
			
		}
		else if (n < 90) {
			return new Tile(Tile.Name.Firehouse);
		}
		else if (n < 100) {
			return new Tile(Tile.Name.Hospital);
		}
		else{
			return new Tile(Tile.Name.Apartment);
		}	
	}
	
	public static void startEvent(GameObject[,] renderedTiles, int tries = 0) {
		int x = Random.Range(0, renderedTiles.GetLength(0));
		int y = Random.Range(0, renderedTiles.GetLength(0));
		bool atLeastOneFine = false;
		for (int i = 0; i < renderedTiles.GetLength(0); i++) {
			for (int j = 0; j < renderedTiles.GetLength(1); j++) {
				if (renderedTiles[i,j].GetComponent<Tile>().status == "fine") {
					atLeastOneFine = true;
				}
			}
		}
		if (atLeastOneFine) {
			if (renderedTiles[x, y].GetComponent<Tile>().status != "fine") {
				startEvent(renderedTiles, tries++);
			}
			else {
				renderedTiles[x, y].GetComponent<Tile>().randomEvent();
			}
		}
			
		
		
	}
	
}
