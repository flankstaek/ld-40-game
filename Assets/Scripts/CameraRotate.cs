﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRotate : MonoBehaviour {
	void Update() {
		transform.RotateAround(Vector3.zero, Vector3.up, 5*Time.deltaTime);
	}
}
