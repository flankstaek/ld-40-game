﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class HighScoreScript : MonoBehaviour {
	public Text recentHighScoreText;
	public Text HighScoreList;
	// Use this for initialization
	void Start () {
		showRecentScore();
		showAllHighScores();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void restartButtonClicked(){
		SceneManager.LoadScene(1);
	}

	public void showRecentScore(){
		recentHighScoreText.text = "Your Score was: " + GameManagerScript.recentHighScore;
	}

	public void showAllHighScores() {
		HighScoreList.text = "";
		if (GameManagerScript.recentHighScore > GameManagerScript.highScores[9]) {
			GameManagerScript.highScores[9] = GameManagerScript.recentHighScore;
		}
		System.Array.Sort(GameManagerScript.highScores);
		System.Array.Reverse(GameManagerScript.highScores);
		for (int i = 0; i < GameManagerScript.highScores.Length; i++) {
			HighScoreList.text += (i+1) +": " + GameManagerScript.highScores[i] + "\n";
			PlayerPrefs.SetInt("Score" + i,GameManagerScript.highScores[i]);
		}
	}
}
